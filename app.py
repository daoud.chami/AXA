#!/usr/bin/env python
"""
File name: app.py

Date created: 2018/07/27

Python Version: 3.6
"""

from flask import Flask, request, jsonify
import os
from werkzeug.utils import secure_filename

from main import engine
from utils.webservice_utils import allowed_file, serialify

UPLOAD_FOLDER = './data/uploads/'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def hello():
    return 'Hello World'


@app.route('/Run', methods=['GET'])
def hello_world():
    return "%s" % engine, 200


@app.route('/predict', methods=['POST'])
def predict():
    # try:
    file = request.files["file"]
    if file and allowed_file(file):
        filename = secure_filename(file.filename)
        tmp_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(tmp_path)
        result = engine.extract(tmp_path)
        print(result)
        dictResult = {i: j for i, j in result[["node", "prediction"]].values}
    return jsonify(dictResult), 200
    # except Exception as e:
    #    return repr(e), 500


@app.route('/predict/batch', methods=['POST'])
def predict_batch():
    try:
        pdf = request.files["file"].read()
        return jsonify(engine.extract(pdf)), 200
    except Exception as e:
        return repr(e), 500


@app.route('/tag', methods=['POST'])
def tag():
    try:
        pdf = request.files["file"].read()
        return jsonify(engine.extract(pdf)), 200
    except Exception as e:
        return repr(e), 500


@app.route('/tag/batch', methods=['POST'])
def tag_batch():
    try:
        pdf = request.files["file"].read()
        return jsonify(engine.extract(pdf)), 200
    except Exception as e:
        return repr(e), 500


@app.route('/score', methods=['POST'])
def score():
    try:
        pdf = request.files["file"].read()
        return jsonify(engine.extract(pdf)), 200
    except Exception as e:
        return repr(e), 500


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)

# gunicorn --bind 0.0.0.0:5000 wsgi
