from sklearn.base import BaseEstimator
from .detectors_conf import ordered_detectors_list, choose


class DummyModel(BaseEstimator):

    def __init__(self,):

        self.detectors = ordered_detectors_list

    def fit(self, X, y=None):
        print("No need to fit")
        pass

    def predict(self, X, y=None):
        predicted_serie = X.apply(
            lambda row: choose([d.activate(row) for d in self.detectors]),
            axis=1
        )
        return predicted_serie


    def prepare(self, data):
        """
        The prepare method is called in the extract method of the engine. Its used to convert a "context graph" dataframe into a
        "learnable" set
        """

        return data

