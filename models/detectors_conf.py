from detectors.DummyDetector import DummyDetector
import re

ebitdaDetector = DummyDetector("EBITDA")
ebitdaDetector.validateNode = lambda x: "$" in " ".join(x["left"] + x["node"]) \
                                        and bool(re.search(r'[,.]', x["node"]))
ebitdaDetector.validateContext = lambda x: "ebit" in x["left"].lower()

revenueDetector = DummyDetector("Revenue")
revenueDetector.validateNode = lambda x: "$" in " ".join(x["left"] + x["node"]) \
                                        and bool(re.search(r'[,.]', x["node"]))
revenueDetector.validateContext = lambda x: "revenue" in x["left"].lower()

capexDetector = DummyDetector("CAPEX")
capexDetector.validateNode = lambda x: "$" in " ".join(x["left"] + x["node"]) \
                                        and bool(re.search(r'[,.]', x["node"]))
capexDetector.validateContext = lambda x: "capex" in x["left"].lower()

ordered_detectors_list = [
    ebitdaDetector,
    revenueDetector,
    capexDetector
]


def choose(detectors_responses_list):
    """
    Analyze detectors answers for a row and return the first not False (otherwise return "OTHER")
    :param detectors_responses_list:
    :return: string: tag
    """
    for resp in detectors_responses_list:
        if resp:
            return resp
    return "OTHER"
