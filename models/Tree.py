from sklearn.base import BaseEstimator
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder
import numpy as np
import pandas as pd
import models.utils as mutils

class Tree(BaseEstimator):

	def __init__(self):
		self.model = DecisionTreeClassifier()
		self.countVectorizer = CountVectorizer(max_features=512)
		self.countVectorizer.preprocessor = mutils.tagNumbers
		self.labelEncoder = LabelEncoder()


	def fit(self, X, y=None):
		self.model.fit(X, y)

	def predict(self, X, y=None):
		return self.model.predict(X)


	def prepare(self, data, prepareTarget=True):
		left = self.countVectorizer.transform(data.left).toarray()
		right = self.countVectorizer.transform(data.right).toarray()
		top = self.countVectorizer.transform(data.top).toarray()
		bottom = self.countVectorizer.transform(data.bottom).toarray()
		featuresNames = [[direction+"_"+word for word in self.countVectorizer.get_feature_names()] for direction in ["left", "right", "top", "bottom"]]
		featuresNames = [y for x in featuresNames for y in x]
		X = np.hstack([left, right, top, bottom])
		preparedData = pd.DataFrame(X, columns=featuresNames)
		if prepareTarget and "target" in data.columns:
			Y = self.labelEncoder.transform(data.target)
			preparedData["Y"] = Y
		return preparedData	

