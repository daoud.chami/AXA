from sklearn.feature_extraction.text import CountVectorizer
import utils
import pandas as pd



def tagNumbers(x):
	"""Replaces all the tokens containing numbers in a string by NUMBEROTKEN
	
	Args:
	    x (str): The string to process
	
	Returns:
	    str: The processed string with NUMBERTOKEN instead of tokens containing numbers
	"""
	return  ' '.join(["NUMBERTOKEN" if utils.utils.hasNumbers(i) else i for i in x.split(" ")]).lower()




def prScore(Y, Yhat, alpha=1.0, beta=1.0, otherClass=3):
	"""The F1 score applied on Y and Yhat
	
	Args:
	    Y (pandas.Series): The pandas.Series of the supervision
	    Yhat (pandas.Sereis): The pandas.Series of the prediction
	    alpha (float, optional): The coefficient of the recall
	    beta (float, optional): The coefficient of the precision
	    otherClass (int, optional): The 'other' class to drop
	
	Returns:
	    float: The F1 score
	"""
	targetedContent = pd.DataFrame()
	
	targetedContent["Y"] = Y
	targetedContent["Yhat"] = Yhat
	targetedContent = targetedContent[targetedContent.Y!=otherClass]
	if len(targetedContent)==0:
		return 0.5
	p = alpha * (targetedContent.Yhat != otherClass).sum() /  len(targetedContent)
	r = beta * (targetedContent.Yhat == targetedContent.Y).sum() / len(targetedContent)
	if p == 0 or r == 0:
		return 0
	return 2 * (p * r) / (p + r)


def wrapperScore(estimator, x, y):
	return prScore(y, estimator.predict(x))
