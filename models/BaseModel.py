from sklearn.base import BaseEstimator
from sklearn.tree import *
from sklearn.feature_extraction.text import *
from sklearn.preprocessing import LabelEncoder
import numpy as np
import pandas as pd
import models.utils as mutils

class BaseModel(BaseEstimator):

    def __init__(self, estimator, vectorizer):
        self.estimator = estimator
        self.vectorizer = vectorizer
        self.vectorizer.preprocessor = mutils.tagNumbers
        self.labelEncoder = LabelEncoder()

    def fit(self, X, y=None):
        self.estimator.fit(X, y)

    def predict(self, X, y=None):
        return self.estimator.predict(X)


    def prepare(self, data, prepareTarget=True):
        left = self.vectorizer.transform(data.left).toarray()
        right = self.vectorizer.transform(data.right).toarray()
        top = self.vectorizer.transform(data.top).toarray()
        bottom = self.vectorizer.transform(data.bottom).toarray()
        featuresNames = [[direction+"_"+word for word in self.vectorizer.get_feature_names()] for direction in ["left", "right", "top", "bottom"]]
        featuresNames = [y for x in featuresNames for y in x]
        X = np.hstack([left, right, top, bottom])
        preparedData = pd.DataFrame(X, columns=featuresNames)
        if prepareTarget and "target" in data.columns:
            Y = self.labelEncoder.transform(data.target)
            preparedData["Y"] = Y
        return preparedData

    def desc(self):
        estimatorDesc = str(self.estimator).replace(" ", "").replace("\n", " ")
        vectorizerDesc = str(self.vectorizer).replace(" ", "").replace("\n", " ")
        return (
            " || ".join([estimatorDesc, vectorizerDesc]),
            self.estimator.__class__.__name__,
            self.vectorizer.__class__.__name__
        )
