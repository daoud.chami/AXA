from builder.reader import Reader
from models import BaseModel
from utils import utils
from models.utils import prScore
import pandas as pd
import os

class Engine() :
    """
    This class manages the reader/contextbuilder and the various detectors
    """

    def __init__(self, reader, model):
        """Builds an Engine. The engine manages the whole data flow : A reader, a model, and the I/O... Its also
        the interface for the flask app.
        
        Args:
            reader (Reader object): An instanciated Reader (containing a ContextBuilder)
            model (Model object): The model to use for the extraction
        """
        self.reader = reader
        self.model = model



    def tag_input_file(self, pdf_path, tag_file_path):
        """Tags an input file (.pdf) given a tag file (.tag)
         
        Args:
            pdf_path (str): The path of the .pdf file to tag
            tag_file_path (str): The path of the .tag file to use
        
        Returns:
            pandas.DataFrame: A pandas.DataFrame where each line is a node and each column a part of its context (left, right, top & bottom)
        """
        contextGraph = self.transform(pdf_path, pages_limit=100)
        tagDict = utils.loadTagFile(tag_file_path)
        return utils.tagData(contextGraph, tagDict)

    def loadTagFiles(self, directory):
        """Calls the .tag_input_file method on a whole directory. This function makes the assumption that for each .pdf file
        a .tag file exists
        
        Args:
            directory (str): The path of the directory to load
        
        Returns:
            pandas.DataFrame: A pandas.DataFrame build on the concatenation of various .tag_input_file returns
        """
        dataSet = []
        for filename in os.listdir(directory):
            path = os.path.join(directory, filename)
            tagFile = '.'.join(path.split(".")[:-1])+".tag"
            if filename.endswith(".pdf") and os.path.isfile(tagFile):
                print(path, tagFile)        
                try:
                    dataSet.append(self.tag_input_file(path,tagFile))
                except Exception as e:
                    print(e)
        return pd.concat(dataSet)



    def extract(self, path):
        """Applies the whole extraction process to a file : the input file is loaded with self.transform. We than call the
        model.prepare method (this makes the assumption that both the model.labelEncoder and model.vectorizer are fitted).
        The produced data set is ready to be used by the model. The prediction is than performed.
        
        Args:
            path (str): The path of the .pdf file to process (predict)
        
        Returns:
            pandas.DataFrame: The output is a pandas.DataFrame with two columns : node and prediction
        """
        inputData = self.transform(path)
        preparedInputData = self.model.prepare(inputData)
        preparedInputData["prediction"] = self.model.labelEncoder.inverse_transform(self.model.predict(preparedInputData))
        preparedInputData["node"] = inputData["node"]
        outputData = preparedInputData[["node", "prediction"]] 
        return outputData[outputData.prediction!="OTHER"]

    def extract_batch(self, directory):
        """Calls the extract method on a directory
        
        Args:
            directory (str): The directory containing the .pdf files to extract
        
        Returns:
            dict: It returns a dictionnary with the paths of the various .pdf as keys and pandas.DataFrame as values.
        """
        extraction = {}
        for filename in os.listdir(directory):
            path = os.path.join(directory, filename)
            if path.endswith(".pdf"):
                extraction[path] = self.extract(path)
        return extraction

    def transform(self, path, pages_limit=0):
        """This method is a wrapper of the Reader.transform method. It will apply self.toSet on the result to produce a pandas.DataFrame
        from the list of dict provided by the Reader.transform method
        
        Args:
            path (str): The path of the .pdf file to transform
            pages_limit (int, optional): The maximum number of page to read from the .pdf
        
        Returns:
            pandas.DataFrame: Returns the application of self.toSet to the result of Reader.transform
        """
        return self.toSet(self.reader.transform(path, pages_limit))  

    def transform_directory(self, path, pages_limit=0):
        """
        calls the transform method for a whole folder
        """
        list_context = list()
        for element in os.listdir(path):
            temp_path = os.path.join(path, element)
            if element[-3:] == 'pdf':
                if os.path.isfile(temp_path):
                    list_context.append(self.transform(temp_path, pages_limit))

            return pd.concat(list_context)

    def toSet(self, contextGraph):
        """Calls the self.flatten method on a contextGraph (a list of dicts)
        
        Args:
            contextGraph (list): A list of dict (each entry is a 'node')
        
        Returns:
            pandas.DataFrame: A pandas.DataFrame where each row is a node flattened (the columns are : node, left, right, top, and bottom - only strings). 
        """
        return pd.DataFrame([self.flatten(x) for x in contextGraph], columns=["node", "left", "right", "top", "bottom"])


    def flatten(self, node):
        """Transforms a single contextGraph (list) entry into a row
        
        Args:
            node (dict): The input node to flatten
        
        Returns:
            tuple: A tuple with the following fields : node.content, left.content, right.content, top.content and bottom.content 
        """
        return (node["node"].content,
        ' '.join([x.content for x in node["left"]]),
        ' '.join([x.content for x in node["right"]]),
        ' '.join([x.content for x in node["top"]]),
        ' '.join([x.content for x in node["bottom"]]))


    def fit_from_directory(self,path):

        data = self.loadTagFiles(path)
        self.fit(data)

    def fit(self, data):
        self.model.vectorizer.fit(data.left+data.right+data.top+data.bottom)
        self.model.labelEncoder.fit(data.target)
        preparedata = self.model.prepare(data)
        self.model.fit(preparedata.drop(columns='Y', axis=1), preparedata.Y)

    def eval_from_directory(self, path):

        data = self.loadTagFiles(path)
        self.eval(data)

    def eval(self, data):

        preparedata = self.model.prepare(data)
        preparedata['Yhat'] = self.model.predict(preparedata.drop(columns='Y', axis=1))
        return prScore(preparedata.Y, preparedata.Yhat)

