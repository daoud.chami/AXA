from models.BaseModel import BaseModel
import time
import pandas as pd
from tqdm import tqdm


class Scheduler:

    def __init__(self, engine, estimators=[], vectorizers=[]):
        """
        Scheduler class : estimate performance of a combination of estimators and vectorizers
        :param engine: engine.Engine
        :param estimators: list of estimators inherited from sklearn.base.BaseEstimator
        :param vectorizers: list of vectorizers
        """
        self.engine = engine
        self.estimators = estimators
        self.vectorizers = vectorizers
        self.verbose = 0

    def run(self, train_set_path, test_set_path, output=None, verbose=0):
        """
        Execute transform, train and evaluate on the combination of estimators and vectorizers
        :param train_set_path: str, path to train set
        :param test_set_path: str, path to test set
        :param output: str, path to output file which will receive results
        :param verbose: int, level of verbosity
        :return: self
        """
        self.verbose = verbose
        self.log("Running scheduler on combination of estimators and vectorizers")
        self.log("--- Estimators ---", v=1)
        self.log([e.__class__.__name__ for e in self.estimators], v=1)
        self.log("--- Vectorizers ---", v=1)
        self.log([v.__class__.__name__ for v in self.vectorizers], v=1)
        columns = ['model_desc', 'estimator', 'vectorizer', 'pr_score', 'time', 'trainset_len', 'testset_len']

        results = pd.DataFrame(columns=columns)

        # Load data
        train_data, test_data = map(self.engine.loadTagFiles, [train_set_path, test_set_path])
        #verbose????
        estimator_iterator = tqdm(self.estimators) if verbose == 0 else self.estimators
        for estimator in estimator_iterator:
            for vectorizer in self.vectorizers:

                # Run estimation
                score = self.estimate(estimator, vectorizer, train_data, test_data)

                # Save results
                description, estimator_name, vectorizer_name = self.engine.model.desc()
                results = results.append(
                    pd.DataFrame(
                        [[description, estimator_name, vectorizer_name, score,  time.ctime(), train_data.shape[0], test_data.shape[0]]],
                        columns=columns
                    ), ignore_index=True
                )
                self.log("--- Results on test set ---", v=1)
                self.log(score, v=1)

        results.sort_values("pr_score", ascending=False, inplace=True)
        self.dump_results(results, output)
        return self

    def estimate(self, estimator, vectorizer, train_data, test_data):
        """
        Run the fit/estimate/score for one couple (estimator, vectorizer)
        :param estimator: sklearn.base.BaseEstimator
        :param vectorizer: vectorizer
        :param train_set_path: str, path to train set
        :param test_set_path: str, path to test set
        :return: score on test set for model fit on train set
        """
        self.log("--- %s - %s ---" % (estimator.__class__.__name__, vectorizer.__class__.__name__), v=1)
        self.engine.model = BaseModel(estimator=estimator, vectorizer=vectorizer)
        self.log("Fitting", v=1)
        self.engine.fit(train_data)
        score = self.engine.eval(test_data)
        return score

    def dump_results(self, results, output):
        if output is not None:
            self.log("Dumping results in %s" % output)
            old_result = pd.read_csv(output, sep=";")
            results = results.append(old_result, ignore_index=True)
            results.to_csv(output, index=False, sep=";")

    def log(self, message, v=0):
        base = "[Scheduler] %s ||" % time.ctime()
        if self.verbose >= v:
            print("%s %s" % (base, message))
