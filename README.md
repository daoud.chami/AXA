## AXA - IM ##
# Text Mining - OCR #



- builder : this package contains the modules and classes required to build a *contextGraph*
- detectors : contains the modules and classes required to detect some elements of a *contextGraph* given some conditions
- engine : applies *detectors* to a *contextGraph*


This project requires **pdfinfo** and **pdftf**.


### Target fields : ###
- EBITDA
- REVENUE
- INCOME
- CAPEX
- CASH
- NET_DEBT

### TODO LIST : ###

- [x] engine.transform_directory
- [x] "vectorizer" as a model's __init__ parameter
- [x] model.estimator as a model's __init__ parameter
- [X] engine.fit_from_directory (transform_directory, fit model.vectorizer and model.labelEncoder, prepare data, fit estimator)
- [X] model.desc (model.estimator desc + model.vectorizer desc) 
- [X] engine.eval_from_directory (mutils.prScore  model.desc)
- [X] GridSeach/Scheduler Class (estimator list + vectorizer list + log file)
- [x] DOCUMENTATION /!\
- [x] engine.fit_from_directory (transform_directory, fit model.vectorizer and model.labelEncoder, prepare data, fit estimator)
- [x] model.desc (model.estimator desc + model.vectorizer desc) 
- [x] engine.eval_from_directory (mutils.prScore  model.desc)
- [x] GridSeach/Scheduler Class (estimator list + vectorizer list + log file)
- [x] DOCUMENTATION /!\
- [X] Improve the Scheduler output (add a date, and both the sizes of the train and test data sets used)
- [X] Historize instead of re-write the output of the Scheduler
- [X] Run at least 100 more experiments (about 10 models and as many vectorizers)
- [ ] Replace imagemagick with pdftoimage (Salotti)
- [ ] Produce a few more samples
- [ ] Produce a post-experiment notebook : how and why a vectorizer/estimator couple will work
- [ ] Find and use a flask's boilerplate to upload .pdfs and see the result from the browser
- [ ] DEPLOY
