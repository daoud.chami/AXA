class DummyDetector:

    def __init__(self, name):
        """This is a Dummy Detector builder. Its not intended to be used
        
        Args:
            name (str): The name of your dummy detector
        """
        self.name = name

    def validateNode(self, node):
        """Checks if a node is valid
        
        Args:
            node (dict): The dictionnary representing the contextualized node
        
        Returns:
            bool: Is the node valid
        """
        return True

    def validateContext(self, context):
        """Checks if a node's context is valid
        
        Args:
            node (dict): The dictionnary representing the contextualized node
        
        Returns:
            bool: Is the node's context valid
        """
        return True

    def preprocess(self, node):
        """Preprocess a node. This method is called before both validateNode & validateContext
        
        Args:
            node (dict): The dictionnary representing the contextualized node
        
        Returns:
            dict: The preprocessed node
        """
        return node

    def postprocess(self, node):
        """Postprocess a node. This method is called after both preprocess, validateNode & validateContext
        
        Args:
            node (dict): The dictionnary representing the contextualized node
        
        Returns:
            dict: The postprocessed node
        """
        return node

    def activate(self, row):
        """Checks if a row (or node) has activated the dummy detector
        
        Args:
            row (dict): he dictionnary representing the contextualized node
        
        Returns:
            bool/string: Returns the name of the dummy detector if the node is validated or False
        """
        prep_row = self.preprocess(row)
        return self.name if self.validateNode(prep_row) and self.validateContext(prep_row) else False

