ALLOWED_EXTENSIONS = ['pdf']


def allowed_file(file):
    return file.filename.split('.')[-1] in ALLOWED_EXTENSIONS


def serialify(dictionary):
    final_dict = {}
    for tag, list_of_elements in dictionary.items():
        final_elements = []
        for element in list_of_elements:
            final_elements.append(
                dict([
                    (
                        feature,
                        box.content if feature == 'node' else [b.content for b in box]
                    ) for feature, box in element.items()
                ])
            )
        final_dict[tag] = final_elements
    return final_dict
