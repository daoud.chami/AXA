SIZE_MULT = 1000
import subprocess
from collections import defaultdict
import os

def pdf_page_count(pdf_path):
    """
    calls the mdls command to count the number of pages of the given pdf file
    """
    try:
        #result = subprocess.check_output('mdls -n kMDItemNumberOfPages '+pdf_path,shell=True,stderr=subprocess.STDOUT)
        result = subprocess.check_output('pdfinfo '+pdf_path+" | grep Pages",shell=True,stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
	
        raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
    return [int(s) for s in result.split() if s.isdigit()][0]

def crop_pdf(pdf_path, pages_limit=10):
    """
    creates a cropped version of the pdf limited to the pages_limit
    first pages. the new file is nammed pdf_path.min.pdf
    """
    os.system("pdftk "+pdf_path+" cat 1-"+str(pages_limit)+" output "+pdf_path+".min.pdf")
    return pdf_path+".min.pdf"


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def loadTagFile(tagFile):
    return defaultdict(lambda : "OTHER", {v:k for k,v in [line.strip().split(" ") for line in open(tagFile,"r").readlines()]})

def tagData(data, tagDict):
    data["target"] = data.node.apply(lambda x : tagDict[x])
    return data  

def overlap1d(line1, line2):
    """
    Detect if two line overlaps
    :param line1: tuple (x_min, x_max)
    :param line2: tuple (x_min, x_max)
    :return: boolean
    """
    return line1[1] >= line2[0] and line2[1] >= line1[0]


def overlap2d(box1, box2):
    """
    Detect if two boxes overlap
    :param box1: tuple of tuples ((x_min, y_min), (x_max, y_max))
    :param box2: tuple of tuples ((x_min, y_min), (x_max, y_max))
    :return: boolean
    """
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1[0] + box1[1]
    box2_x_min, box2_y_min, box2_x_max, box2_y_max = box2[0] + box2[1]
    return (overlap1d((box1_x_min, box1_x_max), (box2_x_min, box2_x_max)) and
            overlap1d((box1_y_min, box1_y_max), (box2_y_min, box2_y_max)))


def combine_boxes(box1, box2):
    """
    merge two boxes into a single one overlapping both box1 and box2
    :param box1: tuple of tuples ((x_min, y_min), (x_max, y_max))
    :param box2: tuple of tuples ((x_min, y_min), (x_max, y_max))
    :return: tuple of tuples ((x_min, y_min), (x_max, y_max))
    """
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1[0] + box1[1]
    box2_x_min, box2_y_min, box2_x_max, box2_y_max = box2[0] + box2[1]
    return ((min([box1_x_min, box2_x_min]), min([box1_y_min, box2_y_min])),
            (max([box1_x_max, box2_x_max]), max([box1_y_max, box2_y_max])))


def right_neighbor(box1, box2):
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1.position[0] + box1.position[1]
    box_width = box1_x_max - box1_x_min
    return overlap2d(((box1_x_min, box1_y_min),
                      (box1_x_max + SIZE_MULT*box_width, box1_y_max)),
                     box2.position)


def left_neighbor(box1, box2):
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1.position[0] + box1.position[1]
    box_width = box1_x_max - box1_x_min
    return overlap2d(((max(box1_x_min - SIZE_MULT*box_width, 0), box1_y_min),
                      (box1_x_max, box1_y_max)),
                     box2.position)


def bottom_neighbor(box1, box2):
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1.position[0] + box1.position[1]
    box_height = box1_y_max - box1_y_min
    return overlap2d(
        ((max(0, box1_x_min - width(box1)), box1_y_min),
         (box1_x_max + width(box1), box1_y_max + SIZE_MULT*box_height)),
        box2.position)


def top_neighbor(box1, box2):
    box1_x_min, box1_y_min, box1_x_max, box1_y_max = box1.position[0] + box1.position[1]
    box_height = box1_y_max - box1_y_min
    return overlap2d(
        ((max(0, box1_x_min - width(box1)), max(0, box1_y_min - SIZE_MULT*box_height)),
         (box1_x_max + width(box1), box1_y_max)),
        box2.position)



def width(box):
    """
    Compute bounding box width
    :return: width (int)
    """
    x_min, y_min, x_max, y_max = box.position[0] + box.position[1]
    return x_max - x_min

def height(box):
    """
    Compute bounding box height
    :return: height (int)
    """
    x_min, y_min, x_max, y_max = box.position[0] + box.position[1]
    return y_max - y_min
