from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import RidgeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.svm import SVR
from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import HuberRegressor
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import GradientBoostingRegressor

estimators = [
    RandomForestClassifier(),
    RandomForestClassifier(max_depth=3),
    RandomForestClassifier(max_depth=5),
    DecisionTreeClassifier(),
    DecisionTreeClassifier(max_depth=3),
    DecisionTreeClassifier(max_depth=5),
    GradientBoostingClassifier(),
    LogisticRegression(),
    RidgeClassifier(),
    KNeighborsClassifier(),
    SVC()
]

vectorizer = {
    CountVectorizer(),
    CountVectorizer(max_features=128),
    CountVectorizer(max_features=256),
    CountVectorizer(max_features=512),
    CountVectorizer(max_features=256, ngram_range=(1, 2)),
    TfidfVectorizer(),
    TfidfVectorizer(max_features=128),
    TfidfVectorizer(max_features=256),
    TfidfVectorizer(max_features=512),
    TfidfVectorizer(max_features=256, ngram_range=(1, 2)),
    RandomForestClassifier(),
    RandomForestClassifier(max_depth=3),
    RandomForestClassifier(max_depth=5),
    DecisionTreeClassifier(),
    DecisionTreeClassifier(max_depth=3),
    DecisionTreeClassifier(max_depth=5),
    LogisticRegression(),
    # BayesianRidge(),
    RidgeClassifier(),
    KNeighborsClassifier(),
    SVC(),
    SVR(),
    SGDClassifier(),
    SGDRegressor(),
    GradientBoostingClassifier(),
    GradientBoostingRegressor(),
    AdaBoostRegressor(),
    AdaBoostClassifier(),
    HuberRegressor()
}

vectorizers = [
    CountVectorizer(),
    CountVectorizer(max_features=128),
    CountVectorizer(max_features=256),
    CountVectorizer(max_features=512),
    CountVectorizer(max_features=256, ngram_range=(1, 2)),
    TfidfVectorizer(),
    TfidfVectorizer(max_features=128),
    TfidfVectorizer(max_features=256),
    TfidfVectorizer(max_features=512),
    TfidfVectorizer(max_features=256, ngram_range=(1, 2))
]
