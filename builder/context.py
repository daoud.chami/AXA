from utils import utils





class ContextBuilder :
    """
    This class defies a ContextBuilder. It's used to produce, given a list of
    terms (position, content) a list of 'contextualized' terms (position, content, [(position,content,relation)]).
    A ContextBuilder object is required to build a Reader object
    """

    def __init__(self, linesLimit=32, validate = lambda x : True):
        """The builder of the ContextBuilder class
        
        Args:
            linesLimit (int, optional): This parameters determines how many lines from tesseract we must fetch above and below
            validate (function, optional): This method filters the node before creating their context
            a box to create its context 
        """
        self.linesLimit = linesLimit
        self.validate = validate


    def create_context(self, termList):
        """This method transforms a tesseract boxes list into a contextualized list of validated nodes (see the builder's doc)
        
        Args:
            termList (list): The tesseract boxes list
        
        Returns:
            list: A contextualized list of contextualized boxes (a dict with the box, and the the following boxes list : left, right, top and bottom)
        """
        contexts = []
        for i in range(len(termList)):
            currentTerm = termList[i]
            if not self.validate(currentTerm):
                continue
            currentContext = termList[max(i-self.linesLimit, 0):i] + termList   [i+1:min(i+self.linesLimit, len(termList))]
            

            contexts.append({
                "node":currentTerm,
                "left":list(filter(lambda x :  utils.left_neighbor(currentTerm,x), currentContext)),
                "right":list(filter(lambda x : utils.right_neighbor(currentTerm,x),currentContext)),
                "top":list(filter(lambda x :  utils.top_neighbor(currentTerm,x),currentContext)),
                "bottom":list(filter(lambda x :  utils.bottom_neighbor(currentTerm,x),currentContext))
                })

        return contexts
