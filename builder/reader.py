from .context import ContextBuilder
import pytesseract
from wand.image import Image
from wand.color import Color
from PIL import Image as PI
import cv2
import numpy as np
import pyocr
from utils import utils
import os.path
import pickle


class Reader :

	def __init__(self, contextBuilder=ContextBuilder()):
		"""The Reader object builder 
		
		Args:
		    contextBuilder (ContextBuilder instance, optional): The context builder that will be used on the tesseract box lists produced
		"""
		self.contextBuilder = contextBuilder

	def process_file(self, path, pages_limit=0):
		"""This method wraps the merge_page and extract_text_boxes methods
		
		Args:
		    path (string): the path of the .pdf file to process
		    pages_limit (int, optional): Maximum number of pages to process
		
		Returns:
		    list: A list of tesseract boxes
		"""
		if os.path.isfile(path+".grad"):
			return pickle.load(open(path+".grad", "rb"))
		else:
			print("Processing ", path, "...")
			image = self.merge_pages(path, 300,  pages_limit=pages_limit)[:32000,:]
			images = [image[(i*32000):((i+1)*32000),:] for i in range(image.shape[0]//32000+1)]
			text_boxes = []
			for image in images:
				if image.shape[0] == 0:
					continue
				text_boxes += self.extract_text_boxes(image)
			pickle.dump(text_boxes, open(path+".grad", "wb"))
			return text_boxes



	def contextualize(self, text_boxes):
		"""Calls the contextBuilder.create_context method on a list of text_boxes
		
		Args:
		    text_boxes (list): A tesseract boxes list
		
		Returns:
		    list: A list of contextualized boxes (see the ContextBuilder.create_context method)
		"""
		return self.contextBuilder.create_context(text_boxes)

	def transform(self, path, pages_limit=0):
		"""This method handles the whole reading/contextualizing process on a single file
		
		Args:
		    path (string): The path of the .pdf file to process
		    pages_limit (int, optional): Maximum number of pages to process
		
		Returns:
		    list: A list of contextualized boxes (see the ContextBuilder.create_context method)
		"""
		return self.contextualize(self.process_file(path, pages_limit))


	def merge_pages(self, fname, dpi, pages_limit=0):
		"""Load and merge PDF pages into a single image. Discar pages of different orientation of size than the first one
		
		Args:
		    fname (string): The path of the .pdf file to process
		    dpi (int): The Dots per inches resolution to read the pdf
		    pages_limit (int, optional): Maximum number of pages to process
		
		Returns:
		    np.ndarray: An image build from the .pdf file
		"""
		print("Loading image...")
		if pages_limit!=0:
			croppedFname = utils.crop_pdf(fname, min(pages_limit, utils.pdf_page_count(fname))) #only the first 5 pages are kept
		else:
			croppedFname = fname
		with Image(filename=croppedFname, resolution=dpi, depth=8) as img:
			print("Done")
			img.background_color = Color('white')
			img.format = 'png'
			img.strip()
			img.compression_quality = 100
			pages = img.sequence
			img.background_color = Color("white")
			img.type = 'grayscale'
			img.alpha_channel = 'remove'
			#pages = list(filter(lambda p: p.height > p.width, pages))
			im_w, im_h = pages[0].width, pages[0].height
			with Image() as blankimage:
				blankimage.blank(im_w, im_h*len(pages))
				blankimage.format = 'png'
				for i, p in enumerate(pages):
					blankimage.composite(p, 0, i*im_h)
				blankimage.background_color = Color("white")
				blankimage.alpha_channel = 'remove'
				blankimage.colorspace = "gray"
				blankimage.type = 'grayscale'
				img_buffer = np.asarray(bytearray(blankimage.make_blob()), dtype=np.uint8)
			os.system("rm "+croppedFname) #the .pdf.min files are removed 
			return cv2.imdecode(img_buffer, cv2.IMREAD_COLOR)[:,:,0]

	def extract_text_boxes(self, img):
		"""Extracts the text boxes from an image
		
		Args:
		    img (numpy.ndarray): The input image from wich the text is extracted
		
		Returns:
		    list: A text boxes list
		"""
		lang, tool = self.load_tesseract()
		text_boxes = tool.image_to_string(
			PI.fromarray(img),
			builder=pyocr.builders.WordBoxBuilder()
		)
		return text_boxes

	def load_tesseract(self):
		"""import tesseract via pyocr
		
		
		Returns:
		    module: Returns the first valid ocr module fetched by pyocr
		"""

		tools = pyocr.get_available_tools()
		if len(tools) == 0:
			print("No OCR tool found")
			sys.exit(1)
		tool = tools[0]
		lang = 'eng'
		return lang, tool
