from flask import Flask, render_template, request
from flask_dropzone import Dropzone
from flask_uploads import UploadSet, configure_uploads, patch_request_class



UPLOAD_FOLDER = './data/uploads/'

app = Flask(__name__)
dropzone = Dropzone(app)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_REDIRECT_VIEW'] = 'results'
app.config['UPLOADED_FILES_DEST'] = './data/flask_save_train'

files = UploadSet('files', extensions=['.pdf', '.txt', '.tag'])
configure_uploads(app, files)
patch_request_class(app)  # set maximum file size, default is 16MB


@app.route('/')
def index():
    return render_template('Test.html')



@app.route('/results', methods=['GET', 'POST'])
def results():
    print(request.method)
    if request.method == 'POST':
        print("hello world")
        file_obj = request.files
        for f in file_obj:
            file = request.files.get(f)
            print(file.filename)
            #photos.save(file, name=file.filename)
    return render_template('Test.html')



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)