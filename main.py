from builder.context import ContextBuilder
from builder.reader import Reader
from engine.engine import Engine
from models.DummyModel import DummyModel
from engine import Engine, Scheduler
from models.BaseModel import BaseModel
# Temporary import to test scheduler
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.tree import DecisionTreeClassifier
import models.utils as mutils
from models.Tree import Tree
import utils
import re
import pandas as pd
import os
import pyocr
import numpy as np
import pickle
import random
import warnings

warnings.filterwarnings(action='ignore', category=DeprecationWarning)


# the pyocr.builder.Box __repr__ method is overrided to gain in readability
#EBITDA, NET LEVERAGE, GROSS LEVERAGE, LTM(LAST TWELVE MONTHS)/REVENUE, NET DEBT, SALES, PROFIT, TOTAL DEBT, CASH

pyocr.builders.Box.__repr__ = lambda x: x.content
	

contextBuilder = ContextBuilder(linesLimit=16)
contextBuilder.validate = lambda x: utils.utils.hasNumbers(x.content)
reader = Reader(contextBuilder=contextBuilder)

engine = Engine(reader=reader, model=pickle.load(open("./model_0.pkl", "rb")))

trainSet = engine.model.prepare(engine.loadTagFiles("./data"))

testSet = engine.model.prepare(engine.loadTagFiles("./data/testFiles"))

# Use case scheduler
#estimators_list = [DecisionTreeClassifier(), DecisionTreeClassifier()]
#vectorizers_list = [CountVectorizer(max_features=512), CountVectorizer(max_features=256)]
scheduler = Scheduler(engine, estimators=experiment.estimators, vectorizers=experiment.vectorizers)
scheduler.run("./data", "./data/testFiles", output="./scheduler_results.csv", verbose=1)

